from django.shortcuts import render, redirect
from authapp.models import UserProfile
from .models import Feedback
from .forms import FeedbackForm

# Create your views here.

def about(request):
    if request.user.is_authenticated:
        allFeedback = Feedback.objects.all()
    if request.user.is_authenticated:
        u = UserProfile.objects.get(user=request.user)
        formFeedback = FeedbackForm(request.POST or None)
        if request.method == 'POST':
            feedback = request.POST['feedback']
            if formFeedback.is_valid() :
                f = Feedback(user=u, feedback=feedback)
                f.save()
                return redirect('/about/')
        
        return render(request, 'about-feedback.html', {'formFeedback': formFeedback, 'allFeedback':allFeedback})

    else:
        return render(request, 'about.html')
from django.db import models
from authapp.models import UserProfile
from django.utils import timezone

# Create your models here.
class Feedback(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True)
    date = models.DateField(default=timezone.now)
    feedback = models.TextField()

    def __str__(self):
        return self.feedback
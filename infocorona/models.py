from django.db import models
from authapp.models import UserProfile
from django.utils import timezone

# Create your models here.
class Feeling(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True)
    date = models.DateField(default=timezone.now)
    feeling = models.TextField()

    def __str__(self):
        return self.feeling
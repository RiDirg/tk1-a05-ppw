from django.shortcuts import render, redirect
from .models import Feeling
from .forms import FeelingForm
from authapp.models import UserProfile

# Create your views here.
def info(request):
    allFeeling = Feeling.objects.all()
    if request.user.is_authenticated:
        u = UserProfile.objects.get(user=request.user)
        formFeeling = FeelingForm(request.POST or None)
        if request.method == 'POST':
            feeling = request.POST['feeling']
            if formFeeling.is_valid() :
                f = Feeling(user=u, feeling=feeling)
                f.save()
                return redirect('/info-corona/')
        
        return render(request, 'info-corona-feel.html', {'formFeeling': formFeeling, 'allFeeling':allFeeling})

    else:
        return render(request, 'info-corona.html')
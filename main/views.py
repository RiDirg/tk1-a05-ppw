from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from authapp.models import UserProfile

def home(request):
    if request.user.is_authenticated:
        name = UserProfile.objects.get(user = request.user)
        data = {
            'name' : name
        }
        return render(request, 'main/home.html', data)
    else:
        return render(request, 'main/home-new-user.html')

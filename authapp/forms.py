from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from . import models

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class  UserProfileForm(ModelForm):
    class Meta:
        model = models.UserProfile
        fields = ['full_name']

        # full_name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'placeholder':'full name'}))
        widget = {
            'full_name': forms.TextInput(attrs={'placeholder': 'name'}),
        }
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class MyFootPrints(models.Model):
    place = models.CharField(max_length=100)
    date = models.DateField()
    desc = models.CharField(max_length=280)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.place